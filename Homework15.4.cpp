﻿#include <iostream>

int const N = 10;

void FindOddNumbers(int Limit, bool IsOdd);

int main()
{
    for(int i=0;i<=N;i+=2)
    {
        std::cout << i;
        std::cout << " ";
    }
    std::cout << "\n";

    FindOddNumbers(N, true);
    std::cout << "\n";
}

void FindOddNumbers(int Limit, bool IsOdd) 
{
    for (int i = IsOdd?1:0 ;i <= Limit;i += 2)
    {
        std::cout << i;
        std::cout << " ";
    }
    std::cout << "\n";
}